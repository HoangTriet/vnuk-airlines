<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update price  n� ${price.id}</h3>

    <form action="../price/update" method="post">
        <input type="hidden" name="id" value="${price.id}" />
        Flight Id: <br/>
        <form:select path = "price.flightId">
        	<form:options items = "${flightList}" />
         </form:select>
        <br/>
        Class Id: <br/>
        <form:select path = "price.classId">
        	<form:options items = "${classList}" />
         </form:select>
       	<br/>
       	Season Id: <br/>
        <form:select path = "price.seasonId">
        	<form:options items = "${seasonList}" />
         </form:select>
       	<br/>
       	Condition Id: <br/>
       	<form:select path = "price.conditionId">
        	<form:options items = "${conditionList}" />
         </form:select>
       	<br/>
       	Price Id: <input type="text" name="price" value="${price.price}"/>
       	<br/>
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../prices">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
