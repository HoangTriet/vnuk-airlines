<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail flight ${flight.id}</h3>

    <form action="../flight/update" method="post">
    	<table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Route</th>
	                <th>Plane</th>
	                <th>Flight Code</th>
	                <th>Day</th>
	                <th>Departure Time</th>
	                <th>Arrival Time</th>
	            </tr>
	        </thead>
	
	        <tbody>
	        	<tr>
	        		<td>${flight.id}</td>
	        		<td>${flight.route.airportFrom.code} - ${flight.route.airportTo.code}</td>
	        		<td>${flight.plane.planeModel.name}</td>
	        		<td>${flight.flightCode}</td>
	        		<td>${flight.day.name}</td>
	        		<td>${flight.departureDate} - ${flight.departureTime}</td>
	        		<td>${flight.arrivalDate} - ${flight.arrivalTime}</td>
	        	</tr>
	        </tbody>
        </table>
		
        <a class="btn btn-default" href="../flights">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
