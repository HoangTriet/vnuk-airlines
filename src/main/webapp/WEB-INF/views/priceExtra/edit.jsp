<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update priceExtra  n� ${priceExtra.id}</h3>

    <form action="../priceExtra/update" method="post">
        <input type="hidden" name="id" value="${priceExtra.id}" />
        Price Id :  <br/>
        <form:select path = "priceExtra.priceId">
        	<form:options items = "${priceList}" />
        </form:select>
        <br />
        Extra Id: <br />
       	<form:select path = "priceExtra.extraId">
        	<form:options items = "${extraList}" />
        </form:select>
        <br />
        Is Included Price : <input type="text" name="isIncludedPrice" value="${priceExtra.isIncludedPrice}"/>
       
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../priceExtras">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
