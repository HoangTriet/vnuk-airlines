<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:import url="/resources/jsp/header_booking.jsp" />

    <h3>Update airport n� ${airport.id}</h3>

    <form action="../airport/update" method="post">
        <input type="hidden" name="id" value="${airport.id}" />
        Code : <br />
        
        <input type="text" name="code" value="${airport.code}" />
        <br /><br />

		City ID : <br />
		<form:select path = "airport.cityId">
         	 <form:options items = "${cityList}" />
          </form:select> 
        <br /><br />
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../airports">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
