<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Show detail ${season.id}</h3>

    <form action="../season/show" method="post">
    	<table class="table table-bordered table-hover table-responsive table-striped">
	        <thead>
	            <tr>
	                <th>Id</th>
	                <th>Name</th>
	            </tr>
	        </thead>
	
	        <tbody>
	        	<tr>
	        		<td>${season.id}</td>
     			   	<td>${season.name}</td>
	        	</tr>
	    	</tbody>
    	</table>
        <a class="btn btn-default" href="../seasons">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Back
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
