<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:import url="/resources/jsp/header_booking.jsp" />
    <h3>Update route  n� ${route.id}</h3>

    <form action="../route/update" method="post">
        <input type="hidden" name="id" value="${route.id}" />
        From Airport Id : <br/>
        	<form:select path = "route.fromAirPortId">
        		<form:options items = "${airportList}" />
        	</form:select>
        <br />
        To Airport Id: <br/>
       	<form:select path = "route.toAirPortId">
        		<form:options items = "${airportList}" />
        	</form:select>
        <br />
       
		<button type="submit" class="btn btn-success">
            <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Update
        </button>
        <a class="btn btn-default" href="../planes">
            <i class="fa fa-times-rectangle-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Cancel
        </a>
    </form>
<c:import url="/resources/jsp/body-close.jsp" />
