
$(function(){

    //  DELETING TASK
    $('.my-priceExtra-to-delete').on('click', function (e){
        e.preventDefault();

        var thisPriceExtra = $(this);
        var priceExtraId = thisPriceExtra.val();

        $.post("priceExtra/delete", {'id' : priceExtraId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "priceExtra/delete",
            
            data: {
                id: priceExtraId
            },
            
            success: function() {
                
            	thisPriceExtra.closest('tr').remove();
                
                $('#my-notice').text('Price Extra ' + priceExtraId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of Price Extra ' + priceExtraId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
