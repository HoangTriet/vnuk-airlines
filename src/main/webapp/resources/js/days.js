
$(function(){

    //  DELETING TASK
    $('.my-day-to-delete').on('click', function (e){
        e.preventDefault();

        var thisDay = $(this);
        var dayId = thisDay.val();

        $.post("day/delete", {'id' : dayId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "day/delete",
            
            data: {
                id: dayId
            },
            
            success: function() {
                
                thisDay.closest('tr').remove();
                
                $('#my-notice').text('Day ' + dayId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of days ' + dayId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
