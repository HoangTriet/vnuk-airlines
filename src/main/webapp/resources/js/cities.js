
$(function(){

    //  DELETING TASK
    $('.my-city-to-delete').on('click', function (e){
        e.preventDefault();

        var thisCity = $(this);
        var cityId = thisCity.val();

        $.post("city/delete", {'id' : cityId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "city/delete",
            
            data: {
                id: cityId
            },
            
            success: function() {
                
                thisCity.closest('tr').remove();
                
                $('#my-notice').text('City ' + cityId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of city ' + cityId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
