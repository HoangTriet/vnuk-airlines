
$(function(){

    //  DELETING TASK
    $('.my-classe-to-delete').on('click', function (e){
        e.preventDefault();

        var thisClasse = $(this);
        var classeId = thisClasse.val();

        $.post("classe/delete", {'id' : classeId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "classe/delete",
            
            data: {
                id: classeId
            },
            
            success: function() {
                
                thisClasse.closest('tr').remove();
                
                $('#my-notice').text('Class' + classeId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of class ' + classeId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
