
$(function(){

    //  DELETING TASK
    $('.my-planeModel-to-delete').on('click', function (e){
        e.preventDefault();

        var thisPlaneModel = $(this);
        var planeModelId = thisPlaneModel.val();

        $.post("planeModel/delete", {'id' : planeModelId}, function() {

        });
        
        $.ajax({
           
            type: "POST",
            url: "planeModel/delete",
            
            data: {
                id: planeModelId
            },
            
            success: function() {
                
            	thisPlaneModel.closest('tr').remove();
                
                $('#my-notice').text('Plane Model ' + planeModelId + ' has successfully been deleted.')
                        .addClass('my-notice-green')
                        .removeClass('my-notice-red');

            },
            
            error: function() {
                $('#my-notice').text('Something went wrong with deletion of Plane Model ' + planeModelId)
                        .removeClass('my-notice-green')
                        .addClass('my-notice-red');
            }
            
        });
        
    });

});
