package vn.edu.vnuk.airlines.dao;


import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Plane;
import vn.edu.vnuk.airlines.model.PlaneModel;

@Repository
public class PlaneDao {
	
	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	 // CREATE
	    public void create(Plane planes) throws SQLException{

	        String sqlQuery = "insert into planes (number_of_seat, plane_model_id)  "
	                        +	"values (?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setInt(1, planes.getNumberOfSeat());
	                statement.setLong(2, planes.getPlaneModelId());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    //  READ (List of Planes)
	    @SuppressWarnings("finally")
	    public List<Plane> read() throws SQLException {

	        String sqlQuery = "select t1.id as plane_id, t1.number_of_seat as number_of_seat, t2.name as planeModel_name from planes t1, plane_models t2 where t1.plane_model_id = t2.id";
	        PreparedStatement statement;
	        List<Plane> planes = new ArrayList<Plane>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Plane plane = new Plane();
	                PlaneModel planeModel = new PlaneModel();
	                
	                plane.setId(results.getLong("plane_id"));
	                plane.setNumberOfSeat(results.getInt("number_of_seat"));
	                plane.setPlaneModel(planeModel);
	                
	                planeModel.setName(results.getString("planeModel_name"));

	                
	                planes.add(plane);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	               
	                return planes;
	        }


	    }
	    //Read single 

	    
	    @SuppressWarnings({ "finally" })
		public Plane read(int id) throws SQLException{

	        String sqlQuery = "select t1.id as plane_id, t1.number_of_seat as number_of_seat, t2.name as planeModel_name from planes t1, plane_models t2 where t1.id=? and t1.plane_model_id = t2.id";

	        PreparedStatement statement;
	        Plane plane = new Plane();
            PlaneModel planeModel = new PlaneModel();
            
	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	plane.setId(results.getLong("plane_id"));
	                plane.setNumberOfSeat(results.getInt("number_of_seat"));
	                plane.setPlaneModel(planeModel);
	                
	                planeModel.setName(results.getString("planeModel_name"));

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            

	            
	            return plane;
	        }

	    
	    }
	    
	//  UPDATE
	    public void update(Plane plane) throws SQLException {
	        String sqlQuery = "update planes set number_of_seat=?, plane_model_id=?" 
	        		 		+" where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setInt(1, plane.getNumberOfSeat());
	            statement.setLong(2, plane.getPlaneModelId());
	            statement.setLong(3, plane.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Planes successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	        finally {
	            
	        }
	        
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from planes where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Planes successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
	    
	    
	    
}