package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Country;

@Repository
public class CountryDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
    }
    public void create(Country countries) throws SQLException{

        String sqlQuery = "insert into countries (name)  "
                        +	"values (?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, countries.getName());
                
               

                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    //  READ (List of PlaneModels)
    @SuppressWarnings("finally")
    public List<Country> read() throws SQLException {

        String sqlQuery = "select * from countries";
        PreparedStatement statement;
        List<Country> countries = new ArrayList<Country>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Country country = new Country();
                country.setId(results.getLong("id"));
                country.setName(results.getString("name"));
                

                
                countries.add(country);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return countries;
        }


    }
    //Read single 
 
    
    @SuppressWarnings({ "finally" })
	public Country read(int id) throws SQLException{

        String sqlQuery = "select * from countries where id=?";

        PreparedStatement statement;
        Country country = new Country();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

            	country.setId(results.getLong("id"));
            	country.setName(results.getString("name"));
                

              

            statement.close();

        } }catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
        	return country;
        }

    
    }
    
//  UPDATE
    public void update(Country countries) throws SQLException {
        String sqlQuery = "update countries set name=? where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, countries.getName());
            statement.setLong(2, countries.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Countries successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

        
    }
    
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from countries where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Country successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    
  
}
