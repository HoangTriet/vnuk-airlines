package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;


import vn.edu.vnuk.airlines.model.Classe;

@Repository
public class ClassDao {
	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	        
	     }
//  CREATE
    public void create(Classe classess) throws SQLException{

        String sqlQuery = "insert into classes (name) "
                        +	"values (?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, classess.getName());


                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
        }

    }
//  READ (List of Class)
    @SuppressWarnings("finally")
    public List<Classe> read() throws SQLException {

        String sqlQuery = "select * from classes";
        PreparedStatement statement;
        List<Classe> classess = new ArrayList<Classe>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Classe classe = new Classe();
                classe.setId(results.getLong("id"));
                classe.setName(results.getString("name"));

                classess.add(classe);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                return classess;
        }
    }
    
    
    @SuppressWarnings({ "finally"})
	public Classe read(int id) throws SQLException{

        String sqlQuery = "select * from classes where id=?";

        PreparedStatement statement;
        Classe classe = new Classe();

        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

                classe.setId(results.getLong("id"));
                classe.setName(results.getString("name"));
               

                
            statement.close();
            }
        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
            return classe;
        }

    }
    
    //  UPDATE
    public void update(Classe classe) throws SQLException {
        String sqlQuery = "update classes set name=? " 
                            + "where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, classe.getName());
            statement.setLong(2, classe.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Classes successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
    }
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from classes where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Classes successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
