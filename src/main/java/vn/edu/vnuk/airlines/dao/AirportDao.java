package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.City;

@Repository
public class AirportDao {
	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
    }
    
//  CREATE
    public void create(Airport airport) throws SQLException{

        String sqlQuery = "insert into airports (code,city_id) "
                        +	"values (?,?)";

        PreparedStatement statement;

        try {
                statement = connection.prepareStatement(sqlQuery);

                //	Replacing "?" through values
                statement.setString(1, airport.getCode());
                statement.setLong(2, airport.getCityId());


                // 	Executing statement
                statement.execute();

                System.out.println("New record in DB !");

        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                System.out.println("Done !");
                
        }

    }
    
//  READ (List of Tasks)
    @SuppressWarnings("finally")
    public List<Airport> read() throws SQLException {

        String sqlQuery = "select t1.id as airport_id, t1.code as airport_code, t2.name as city_name from airports t1, cities t2 where t1.city_id = t2.id";
        PreparedStatement statement;
        List<Airport> airports = new ArrayList<Airport>();

        try {

            statement = connection.prepareStatement(sqlQuery);

            // 	Executing statement
            ResultSet results = statement.executeQuery();
            
            while(results.next()){

                Airport airport = new Airport();
                City city = new City();
                
                airport.setId(results.getLong("airport_id"));
                airport.setCode(results.getString("airport_code"));
                airport.setCity(city);

                city.setName(results.getString("city_name"));
                
                airports.add(airport);

            }

            results.close();
            statement.close();


        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {
                
                return airports;
        }
    }
//  READ (Single City)
 
    
    
    @SuppressWarnings({ "finally"})
	public Airport read(int id) throws SQLException{

        String sqlQuery = "select t1.id as airport_id, t1.code as airport_code, t2.name as city_name from airports t1, cities t2 where t1.id=? and t1.city_id = t2.id";

        PreparedStatement statement;
        Airport airport = new Airport();
        City city = new City();
        
        try {
            statement = connection.prepareStatement(sqlQuery);

            //	Replacing "?" through values
            statement.setLong(1, id);

            // 	Executing statement
            ResultSet results = statement.executeQuery();

            if(results.next()){

            	airport.setId(results.getLong("airport_id"));
                airport.setCode(results.getString("airport_code"));
                airport.setCity(city);

                city.setName(results.getString("city_name"));
               
                
            statement.close();
            }
        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        } finally {

            
            return airport;
        }

    }
//  UPDATE
    public void update(Airport airport) throws SQLException {
        String sqlQuery = "update airports set code=?, city_id=?" 
                            + " where id=?";
        
        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setString(1, airport.getCode());
            statement.setLong(2, airport.getCityId());
            statement.setLong(3, airport.getId());
            statement.execute();
            statement.close();
            
            System.out.println("Airports successfully modified.");
        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

        
    }
//  DELETE
    public void delete(int id) throws SQLException {
        String sqlQuery = "delete from airports where id=?";

        try {
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            statement.execute();
            statement.close();
            
            System.out.println("Airports successfully deleted.");

        } 

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        


    }
}
