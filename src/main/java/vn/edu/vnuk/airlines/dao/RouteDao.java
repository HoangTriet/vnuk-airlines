package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.Route;


@Repository
public class RouteDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	 // CREATE
	    public void create(Route routes) throws SQLException{

	        String sqlQuery = "insert into routes (from_airport_id, to_airport_id)  "
	                        +	"values (?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setLong(1, routes.getFromAirPortId());
	                statement.setLong(2, routes.getToAirPortId());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    
	    //  READ (List of Routes)
	    @SuppressWarnings("finally")
	    public List<Route> read() throws SQLException {

	        String sqlQuery = "select r.id as route_id, from_a.code as airport_from, to_a.code as airport_to, from_airport_id, to_airport_id "
	        		+ "	from routes r, airports from_a, airports to_a "
	        		+ " where r.from_airport_id = from_a.id and r.to_airport_id = to_a.id;";
	        PreparedStatement statement;
	        List<Route> routes = new ArrayList<Route>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Route route = new Route();
	                Airport fromAirport = new Airport();
	                Airport toAirport = new Airport();
	                route.setId(results.getLong("route_id"));
	                route.setAirportFrom(fromAirport);
	                route.setAirportTo(toAirport);
	                route.setFromAirPortId(results.getLong("from_airport_id"));
	                route.setToAirPortId(results.getLong("to_airport_id"));
	                
	                fromAirport.setCode(results.getString("airport_from"));
	                toAirport.setCode(results.getString("airport_to"));
	                
	                routes.add(route);

	            }

	            results.close();
	            statement.close();


	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	               
	                return routes;
	        }


	    }
	    
	    //Read single 

	    
	    @SuppressWarnings({ "finally" })
		public Route read(int id) throws SQLException{

	        String sqlQuery = "select r.id as route_id, from_a.code as airport_from, to_a.code as airport_to from routes r, airports from_a, airports to_a where r.id=? and r.from_airport_id = from_a.id and r.to_airport_id = to_a.id;";

	        PreparedStatement statement;
	        Route route = new Route();
	        Airport fromAirport = new Airport();
            Airport toAirport = new Airport();
	        
	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	route.setId(results.getLong("route_id"));
	                route.setAirportFrom(fromAirport);
	                route.setAirportTo(toAirport);
	                
	                
	                fromAirport.setCode(results.getString("airport_from"));
	                toAirport.setCode(results.getString("airport_to"));

	              

	            statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            

	            
	            return route;
	        }

	    
	    }
	    
	//  UPDATE
	    public void update(Route route) throws SQLException {
	        String sqlQuery = "update routes set from_airport_id=?, to_airport_id=?" 
	        		 		+" where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, route.getFromAirPortId());
	            statement.setLong(2, route.getToAirPortId());
	            statement.setLong(3, route.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Routes successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        
	        finally {
	            
	        }
	        
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from routes where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Routes successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
}
