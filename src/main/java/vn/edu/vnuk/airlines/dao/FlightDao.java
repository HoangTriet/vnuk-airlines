package vn.edu.vnuk.airlines.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.stereotype.Repository;

import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.Day;
import vn.edu.vnuk.airlines.model.Flight;
import vn.edu.vnuk.airlines.model.Plane;

import vn.edu.vnuk.airlines.model.PlaneModel;
import vn.edu.vnuk.airlines.model.Route;



@Repository
public class FlightDao {

	private Connection connection;

	 public void setDataSource(DataSource dataSource) {
	        
	        try {
	 	    	this.connection = dataSource.getConnection();
	 	    } catch (SQLException e) {
	 	    	throw new RuntimeException(e);
	 	    }
	    }
	 
	 // CREATE
	    public void create(Flight flights) throws SQLException{

	        String sqlQuery = "insert into flights (route_id, plane_id, day_id, departure_time, arrival_time, flight_code)  "
	                        +	"values (?, ?, ?, ?, ?, ?)";

	        PreparedStatement statement;

	        try {
	                statement = connection.prepareStatement(sqlQuery);

	                //	Replacing "?" through values
	                statement.setLong(1, flights.getRouteId());
	                statement.setLong(2, flights.getPlaneId());
	                statement.setLong(3, flights.getDayId());
	                statement.setTime(4, flights.getDepartureTime());
	                statement.setTime(5, flights.getArrivalTime());
	                statement.setString(6, flights.getFlightCode());
	               

	                // 	Executing statement
	                statement.execute();

	                System.out.println("New record in DB !");

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	                System.out.println("Done !");
	        }

	    }
	    
	//  READ (List of PlaneModels)
	    @SuppressWarnings("finally")
	    public List<Flight> read() throws SQLException {

	        String sqlQuery = "select f.id as flight_id, f.route_id as flight_route, f.departure_time as d_time, f.arrival_time as a_time, f.flight_code as f_code, "
	        				+ "p.plane_model_id as planeModel_id, p_model.name as plane_name, d.name as day_name, "
	        				+ "r.id as route_id, from_a.code as from_airport, to_a.code as to_airport, DATE(departure_time) as date_in_departure, DATE(arrival_time) as date_in_arrival "
	        				+ "from flights f, planes p, plane_models p_model, days d, plane_manufactures p_manufacture, routes r, airports from_a, airports to_a "
	        				+ "where f.plane_id = p.id and p.plane_model_id = p_model.id and p_model.plane_manufacture_id = p_manufacture.id and f.day_id = d.id and "
	        				+ "f.route_id = r.id and r.from_airport_id = from_a.id and r.to_airport_id = to_a.id;";
	        PreparedStatement statement;
	        List<Flight> flights = new ArrayList<Flight>();

	        try {

	            statement = connection.prepareStatement(sqlQuery);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();
	            
	            while(results.next()){

	                Flight flight = new Flight();
	                PlaneModel planeModel = new PlaneModel();
	                Plane plane = new Plane();
	                Day day = new Day();
	                Route route = new Route();
	                Airport airportFrom = new Airport();
	                Airport airportTo = new Airport();
	                
	                flight.setId(results.getLong("flight_id"));
	                flight.setDepartureTime(results.getTime("d_time"));
	                flight.setArrivalTime(results.getTime("a_time"));
	                flight.setDepartureDate(results.getDate("date_in_departure"));
	                flight.setArrivalDate(results.getDate("date_in_arrival"));
	                flight.setFlightCode(results.getString("f_code"));
	                flight.setRouteId(results.getLong("flight_route"));
	                flight.setPlane(plane);
	                flight.setDay(day);
	                flight.setRoute(route);
	                
	                planeModel.setName(results.getString("plane_name"));
	                airportFrom.setCode(results.getString("from_airport"));
	                airportTo.setCode(results.getString("to_airport"));
	                day.setName(results.getString("day_name"));
	                plane.setPlaneModel(planeModel);
	                route.setAirportFrom(airportFrom);
	                route.setAirportTo(airportTo);
	                
	                flights.add(flight);
	            }

	            results.close();
	            statement.close();

	        } catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	               
	        	return flights;
	        }
	    }
	    
	    //Read single 
	    @SuppressWarnings({ "finally" })
		public Flight read(int id) throws SQLException{

	        String sqlQuery = "select f.id as flight_id, f.route_id as flight_route, f.departure_time as d_time, f.arrival_time as a_time, f.flight_code as f_code, "
	        				+ "p.plane_model_id as planeModel_id, p_model.name as plane_name, d.name as day_name, "
	        				+ "r.id as route_id, from_a.code as from_airport, to_a.code as to_airport, DATE(departure_time) as date_in_departure, DATE(arrival_time) as date_in_arrival "
	        				
	        				+ "from flights f, planes p, plane_models p_model, days d, plane_manufactures p_manufacture, routes r, airports from_a, airports to_a "
	        				+ "where f.id=? and f.plane_id = p.id and p.plane_model_id = p_model.id and p_model.plane_manufacture_id = p_manufacture.id and f.day_id = d.id and "
	        				+ "f.route_id = r.id and r.from_airport_id = from_a.id and r.to_airport_id = to_a.id;";

	        PreparedStatement statement;
	        Flight flight = new Flight();
            PlaneModel planeModel = new PlaneModel();
            Plane plane = new Plane();
            Day day = new Day();
            Route route = new Route();
            Airport airportFrom = new Airport();
            Airport airportTo = new Airport();
	        try {
	            statement = connection.prepareStatement(sqlQuery);

	            //	Replacing "?" through values
	            statement.setLong(1, id);

	            // 	Executing statement
	            ResultSet results = statement.executeQuery();

	            if(results.next()){

	            	flight.setId(results.getLong("flight_id"));
	                flight.setDepartureTime(results.getTime("d_time"));
	                flight.setArrivalTime(results.getTime("a_time"));
	                flight.setDepartureDate(results.getDate("date_in_departure"));
	                flight.setArrivalDate(results.getDate("date_in_arrival"));
	                flight.setFlightCode(results.getString("f_code"));
	                flight.setRouteId(results.getLong("flight_route"));
	                flight.setPlane(plane);
	                flight.setDay(day);
	                flight.setRoute(route);
	                
	                planeModel.setName(results.getString("plane_name"));
	                airportFrom.setCode(results.getString("from_airport"));
	                airportTo.setCode(results.getString("to_airport"));
	                day.setName(results.getString("day_name"));
	                plane.setPlaneModel(planeModel);
	                route.setAirportFrom(airportFrom);
	                route.setAirportTo(airportTo);

	                statement.close();

	        } }catch (Exception e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	        } finally {
	            
	            return flight;
	        }
	    }
	    
	    //  UPDATE
	    public void update(Flight flight) throws SQLException {
	        String sqlQuery = "update flights set route_id=?, plane_id=?, day_id=?, departure_time=?, arrival_time=?, flight_code=?" 
	        		 		+" where id=?";
	        
	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, flight.getRouteId());
                statement.setLong(2, flight.getPlaneId());
                statement.setLong(3, flight.getDayId());
                statement.setTime(4, flight.getDepartureTime());
                statement.setTime(5, flight.getArrivalTime());
                statement.setString(6, flight.getFlightCode());
                statement.setLong(7, flight.getId());
	            statement.execute();
	            statement.close();
	            
	            System.out.println("flights successfully modified.");
	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	    }
	    
	//  DELETE
	    public void delete(int id) throws SQLException {
	        String sqlQuery = "delete from flights where id=?";

	        try {
	            PreparedStatement statement = connection.prepareStatement(sqlQuery);
	            statement.setLong(1, id);
	            statement.execute();
	            statement.close();
	            
	            System.out.println("Flights successfully deleted.");

	        } 

	        catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }

	    }
}
