package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5030_InsertSeasons {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5030_InsertSeasons(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into seasons (name) "
				+ "values ('spring'), ('summer'), ('fall'), ('winter')";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table seasons in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
