package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_5110_InsertRoutes {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5110_InsertRoutes(Connection connection) {
		this.connection = connection;
		this.sqlQuery = "insert into routes (from_airport_id,to_airport_id) "
				+ "values(1, 2), (1, 3), (2, 3), (1, 5), "
				+ "(2, 4), (3, 4)";
	}
	
	public void run() throws SQLException {

		try {
		        connection.prepareStatement(sqlQuery).execute();
		        System.out.println("New data in table routes in DB !");
		
		} catch (Exception e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		} finally {
		        System.out.println("Done !");
		        //connection.close();
		}
	}
}
