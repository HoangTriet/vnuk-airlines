package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5050_InsertClasses {

	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5050_InsertClasses(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into classes (name) "
				+ "values ('Economy'), ('Business'), ('VIP')";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table classes in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
