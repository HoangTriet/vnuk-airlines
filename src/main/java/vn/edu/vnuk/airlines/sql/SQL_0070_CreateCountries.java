package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0070_CreateCountries {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0070_CreateCountries(Connection connection) {
//		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS countries ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "name VARCHAR(100), "
						+ "primary key (id)"
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table countries in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}
