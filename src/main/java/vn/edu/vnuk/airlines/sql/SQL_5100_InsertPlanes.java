package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;



public class SQL_5100_InsertPlanes {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5100_InsertPlanes(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into planes (number_of_seat,plane_model_id) "
				+ "values(179, 1), (440, 2), (305, 3), (853, 4), "
				+ "(177, 5), (550, 6), (257,7) ";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table planes in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
