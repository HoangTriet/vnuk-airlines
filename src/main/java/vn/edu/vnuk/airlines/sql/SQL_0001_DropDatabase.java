package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0001_DropDatabase {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0001_DropDatabase(Connection connection) {
		this.connection = connection;
		this.sqlQuery = "drop database IF EXISTS vnuk_airlines;";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("Drop database vnuk_airlines !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			}
			
	}
}
