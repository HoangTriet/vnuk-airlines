package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;

public class SQL_0110_CreateRoutes {
	
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_0110_CreateRoutes(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "create table IF NOT EXISTS routes ("
						+ "id BIGINT NOT NULL AUTO_INCREMENT, "
						+ "from_airport_id BIGINT NOT NULL, "
						+ "to_airport_id BIGINT NOT NULL,"
						+ "primary key (id),"
						+ "foreign key (from_airport_id) "
						+ "REFERENCES airports(id), "
						+ "foreign key (to_airport_id) "
						+ "REFERENCES airports(id) "
						+ ");";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New table routes in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
		}
	
}