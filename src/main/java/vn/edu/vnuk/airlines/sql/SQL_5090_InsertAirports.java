package vn.edu.vnuk.airlines.sql;

import java.sql.Connection;
import java.sql.SQLException;


public class SQL_5090_InsertAirports {
	private final Connection connection;
	private final String sqlQuery;
	
	public SQL_5090_InsertAirports(Connection connection) {
		super();
		this.connection = connection;
		this.sqlQuery = "insert into airports (code,city_id) "
				+ "values('HAN', 1), ('SGN', 2), ('DAD', 3), ('VCA', 4), "
				+ "('SEOA', 5)";
	}
	
	public void run() throws SQLException {

			try {
			        connection.prepareStatement(sqlQuery).execute();
			        System.out.println("New data in table airports in DB !");
			
			} catch (Exception e) {
			        // TODO Auto-generated catch block
			        e.printStackTrace();
			} finally {
			        System.out.println("Done !");
			        //connection.close();
			}
			
	}
	
	
}
