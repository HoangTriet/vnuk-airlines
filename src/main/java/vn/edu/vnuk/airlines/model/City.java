package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class City{
	private long id;
	private String name;
	private long countryId;
	private Country country;
	
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}
	@NotNull
	public long getId(){
		return id;
		}
	public String getName(){
		return name;
		}
	public long getCountryId(){
		return countryId;
		}
		
	public void setId(long id){
		this.id = id;
		}
	public void setName(String name){
		this.name = name;
		}
	public void setCountryId(long countryId){
		this.countryId = countryId;
		}

}