package vn.edu.vnuk.airlines.model;

import javax.validation.constraints.NotNull;

public class Plane{
	private long id;
	private int numberOfSeat;
	private long planeModelId;
	private PlaneModel planeModel;
	
	public PlaneModel getPlaneModel() {
		return planeModel;
	}
	public void setPlaneModel(PlaneModel planeModel) {
		this.planeModel = planeModel;
	}
	@NotNull
	public long getId(){
		return id;
		}
	public int getNumberOfSeat(){
		return numberOfSeat;
		}
	public long getPlaneModelId(){
		return planeModelId;
		}
		
	public void setId(long id){
		this.id = id;
		}
	public void setNumberOfSeat(int numberOfSeat){
		this.numberOfSeat = numberOfSeat;
		}
	public void setplaneModelId(long planeModelId){
		this.planeModelId = planeModelId;
		}
}
