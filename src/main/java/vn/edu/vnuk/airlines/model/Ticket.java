package vn.edu.vnuk.airlines.model;

import java.sql.Time;
import java.util.Date;

public class Ticket {
	
//	private String name;
	private Route route;
	private long planeId;
	private Date arrivalDate;
	private Date departureDate;
	private Time arrivalTime;
	private Time departureTime;
	private String flightCode;
	private double price;
	private long fromCityId;
	private long toCityId;
	private String conditionDes;
	private String seasonName;
	private String className;
	private String planeName;
	
	public String getPlaneName() {
		return planeName;
	}
	public void setPlaneName(String planeName) {
		this.planeName = planeName;
	}
	
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public long getPlaneId() {
		return planeId;
	}
	public void setPlaneId(long planeId) {
		this.planeId = planeId;
	}
	public String getFlightCode() {
		return flightCode;
	}
	public void setFlightCode(String flightCode) {
		this.flightCode = flightCode;
	}
	public long getFromCityId() {
		return fromCityId;
	}
	public void setFromCityId(long fromCityId) {
		this.fromCityId = fromCityId;
	}
	public long getToCityId() {
		return toCityId;
	}
	public void setToCityId(long toCityId) {
		this.toCityId = toCityId;
	}
	public String getConditionDes() {
		return conditionDes;
	}
	public void setConditionDes(String conditionDes) {
		this.conditionDes = conditionDes;
	}
	public String getSeasonName() {
		return seasonName;
	}
	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Time getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Time arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public Time getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Time departureTime) {
		this.departureTime = departureTime;
	}
	
}
