package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.AirportDao;
import vn.edu.vnuk.airlines.dao.CityDao;
import vn.edu.vnuk.airlines.dao.DayDao;
import vn.edu.vnuk.airlines.dao.FlightDao;

import vn.edu.vnuk.airlines.dao.PlaneModelDao;
import vn.edu.vnuk.airlines.dao.RouteDao;
import vn.edu.vnuk.airlines.model.Airport;
import vn.edu.vnuk.airlines.model.City;
import vn.edu.vnuk.airlines.model.Day;
import vn.edu.vnuk.airlines.model.Flight;

import vn.edu.vnuk.airlines.model.PlaneModel;
import vn.edu.vnuk.airlines.model.Route;


@Controller
public class FlightsController {

	private final FlightDao flightDao;
	private final RouteDao routeDao;
	private final DayDao dayDao;
	private final PlaneModelDao planeModelDao;
	private final AirportDao airportDao;
	private final CityDao cityDao;
	
	@Autowired
	public FlightsController(FlightDao flightDao, RouteDao routeDao, DayDao dayDao, PlaneModelDao planeModelDao, AirportDao airportDao,CityDao cityDao) {
		this.flightDao = flightDao;
		this.routeDao = routeDao;
		this.dayDao = dayDao;
		this.planeModelDao = planeModelDao;
		this.airportDao = airportDao;
		this.cityDao = cityDao;
	}
	
	@RequestMapping("flight/new")
    public String add(Model model){
		model.addAttribute("flight", new Flight());
        return "flight/new";
	}
	
	@RequestMapping("flight/create")
    public String create(@Valid Flight flight, BindingResult result) throws SQLException{
        flightDao.create(flight);
        return "redirect:/flights";
	}
	
	@RequestMapping("flights")
    public String read(Model model) throws SQLException{
        model.addAttribute("flights", flightDao.read());
        return "flight/index";
	}
	
	@RequestMapping("flight/show")
    public String show(@RequestParam Map<String, String> flightId, Model model) throws SQLException{
        int id = Integer.parseInt(flightId.get("id").toString());
        model.addAttribute("flight", flightDao.read(id));
        return "flight/show";
    }
	
	@RequestMapping("flight/edit")
    public String edit(@RequestParam Map<String, String> flightId, Model model) throws SQLException{
        int id = Integer.parseInt(flightId.get("id").toString());
        model.addAttribute("flight", flightDao.read(id));
        return "flight/edit";
    }
	
	@RequestMapping("flight/update")
    public String update(@Valid Flight flight, BindingResult result) throws SQLException{

        flightDao.update(flight);
        return "redirect:/flights";
    }
	
	//  DELETE WITH AJAX
    @RequestMapping(value="flight/delete", method = RequestMethod.POST)
    public void delete(int id, HttpServletResponse response) throws SQLException {
        flightDao.delete(id);
        response.setStatus(200);
    }
    
    @ModelAttribute("planeModelList")
    public Map<Long, String> getPlaneModels(){
    	Map<Long, String> planeModelList = new HashMap<Long, String>();
    	try {
    		for(PlaneModel planeModel : planeModelDao.read()) {
    			planeModelList.put(planeModel.getId(), planeModel.getName());
    		}
    	}catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return planeModelList;
    }
    
    @ModelAttribute("routeList")
    public Map<Long, String> getRoutes(){
    	String fromCity = "";
    	String toCity= "";
    	String getCityInString = "";
		
    	Map<Long, String> routeList = new HashMap<Long, String>();
    	try {
    		for(Route route : routeDao.read()) {
    			for(Airport airport : airportDao.read()) {
    				for(City city : cityDao.read()) {
	    				if(route.getFromAirPortId() == airport.getId() ) {
	    					if(airport.getId() == city.getId())
	    						fromCity = city.getName();
	    				}
	    				if(route.getToAirPortId() == airport.getId()) {
	    					if(airport.getId() == city.getId())
	    						toCity = city.getName();
	    				}
	    				getCityInString = fromCity + " - " + toCity;
	    				routeList.put(route.getId(), getCityInString);
    				}
    			}
    		}
    	}catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return routeList;
    }
    
    @ModelAttribute("dayList")
    public Map<Long, String> getDays(){
    	Map<Long, String> dayList = new HashMap<Long, String>();
    	try {
    		for(Day day : dayDao.read()) {
    			dayList.put(day.getId(), day.getName());
    		}
    	}catch (SQLException e) {
    		e.printStackTrace();
    	}
    	return dayList;
    }
    
}
