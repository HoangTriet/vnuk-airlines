package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.edu.vnuk.airlines.dao.PlaneDao;
import vn.edu.vnuk.airlines.dao.PlaneModelDao;
import vn.edu.vnuk.airlines.model.Plane;
import vn.edu.vnuk.airlines.model.PlaneModel;

@Controller
public class PlanesController {

	private final PlaneDao planeDao;
	private final PlaneModelDao planeModelDao;
	
	@Autowired
	public PlanesController(PlaneDao planeDao, PlaneModelDao planeModelDao) {
		this.planeDao = planeDao;
		this.planeModelDao = planeModelDao;
	}
		
	 @RequestMapping("plane/new")
	    public String add(Model model){
		 	model.addAttribute("plane", new Plane());
	        return "plane/new";
	 }
	
	 @RequestMapping("plane/create")
	    public String create(@Valid Plane plane, BindingResult result) throws SQLException{
	        planeDao.create(plane);
	        return "redirect:/planes";
	 }
	 
	 @RequestMapping("planes")
	    public String read(Model model) throws SQLException{
	        model.addAttribute("planes", planeDao.read());
	        return "plane/index";
	 }
	 
	 @RequestMapping("plane/show")
	    public String show(@RequestParam Map<String, String> planeId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeId.get("id").toString());
	        model.addAttribute("plane", planeDao.read(id));
	        return "plane/show";
	    }
	 
	 @RequestMapping("plane/edit")
	    public String edit(@RequestParam Map<String, String> planeId, Model model) throws SQLException{
	        int id = Integer.parseInt(planeId.get("id").toString());
	        model.addAttribute("plane", planeDao.read(id));
	        return "plane/edit";
	    }
	 
	 @RequestMapping("plane/update")
	    public String update(@Valid Plane plane, BindingResult result) throws SQLException{
	        planeDao.update(plane);
	        return "redirect:/planes";
	    }
	//  DELETE WITH AJAX
	    @RequestMapping(value="plane/delete", method = RequestMethod.POST)
	    public void delete(int id, HttpServletResponse response) throws SQLException {
	        planeDao.delete(id);
	        response.setStatus(200);
	    }
	    
	    @ModelAttribute("planeModelList")
	    public Map<Long, String> getPlaneModels(){
	    	Map<Long, String> planeModelList = new HashMap<Long, String>();
	    	try {
	    		for(PlaneModel planeModel : planeModelDao.read()) {
	    			planeModelList.put(planeModel.getId(), planeModel.getName());
	    		}
	    	}catch (SQLException e) {
	    		e.printStackTrace();
	    	}
	    	return planeModelList;
	    }
	    
}
