package vn.edu.vnuk.airlines.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import vn.edu.vnuk.airlines.dao.CityDao;
import vn.edu.vnuk.airlines.dao.TicketDao;
import vn.edu.vnuk.airlines.model.City;
import vn.edu.vnuk.airlines.model.Ticket;

@Controller
public class WelcomeController {
	private final TicketDao ticketDao;
	private final CityDao cityDao;
	
	
	@Autowired
	public WelcomeController(TicketDao ticketDao, CityDao cityDao) {
		this.ticketDao = ticketDao;
		this.cityDao = cityDao;
	}
	
	@RequestMapping({"", "/"})
	public String home(Model model) {
		
		model.addAttribute("ticket", new Ticket());
		return "welcome/welcome";
	}
	
	@RequestMapping("bookTicket")
	public String read(Model model) throws SQLException{
      model.addAttribute("tickets", ticketDao.read());
      return "ticket/index";
	}
	
	@ModelAttribute("cityList")
    public Map<Long, String> getCountries(){
    	Map<Long, String> cityList = new HashMap<Long, String>();
    	try {
    		for(City city : cityDao.read()) {
    			cityList.put(city.getId(), city.getName());
    		}
    	}  catch (SQLException e) {
    		 // TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	return cityList;
    }
}
